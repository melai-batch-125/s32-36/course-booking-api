const User = require("./../models/User");
const bcrypt = require("bcrypt");
const auth = require("./../auth");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email })
	.then((result) => {
		if(result.length != 0)
			return true; //existing
		else {
			return false; //non existing
		}

	});
};


module.exports.register = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	});

	return newUser.save().then( (result, error) => {
		if(error){
			return error;
		} else {
			return true;
		}
	});

};

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect === true) {
				return {access: auth.createAccessToken(result.toObject())};
			} else {
				return false;
			}
		}
	})
};

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		result.password = "*****";
		return result;
	});
};