let jwt = require('jsonwebtoken');
let secret = "CourseBookingAPI";


//create token
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if (typeof token !== "undefined") {
		token = token.slice(7);

		return jwt.verify(token, secret, (error,data) => {
			if (error) {
				return res.send()
			} else {
				next();
			}
		});
	}
};
//decode token

module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7);

		return jwt.verify(token, secret, (error,data) => {
			if (error) {
				return null;
			} else {
				return jwt.decode(token, {complete:true}).payload;
			}
		});
	}
};

