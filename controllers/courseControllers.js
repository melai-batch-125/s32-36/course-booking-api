const  Course = require('./../models/Course');


module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then( result => {
		return result;
	} )
}


module.exports.getAllCourse = () => {
	return Course.find().then( result => {
		return result;
	} )
}



module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then( (result, error) => {
		if(error){
			return error;
		} else {
			return true;
		}
	});

};

module.exports.getSingleCourse = (params) => {
	return Course.findById(params.courseId).then( result => {
		return course;
	} )
}


//edit course
module.exports.editCourse = (params, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//Model.method
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}