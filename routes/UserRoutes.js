const express = require(`express`);
const router = express.Router();

const userController = require("./../controllers/userControllers");
const auth = require("./../auth.js");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

router.post("/register", (req,res) => {
	userController.register(req.body).then(result => res.send(result))
});

router.post("/login", (req,res) => {
	userController.login(req.body).then(result => res.send(result))
});

router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(userData.id).then(result => res.send(result))
});

module.exports = router;

/*router.get("/", (req,res)=>{
	userController.getAll().then(result => res.send(result))

});*/