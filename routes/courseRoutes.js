const express = require("express");
const router = express.Router();


//controllers
const courseController = require("./../controllers/courseControllers");
//retrieve all active courses
router.get('/', (req,res) => {
	courseController.getAllActive().then(result => res.send(result));
})

//retrieve all courses
router.get('/all', (req,res) => {
	courseController.getAllCourse().then(result => res.send(result));
})

//add courses
router.post('/addCourse', (req,res) => {
	courseController.addCourse(req.body).then(result => res.send(result));
})


router.get('/:courseId', auth.verify, (req,res) => {
	courseController.getSingleCourse(req.params).then(result => res.send(result))
})





module.exports = router;

